# THE REPOSITORY WAS MIGRATED TO GITHUB!
[**NEW GITHUB REPOSITORY**](https://github.com/OpenBankingUK/uml-generator)  
The BitBucket repository will be deleted in due time.
---

# UML Generator

Utility for generating SVG diagrams from YAML files containing JSON schema descriptions.

## Command Line Utility

Generates UML diagrams from an OpenAPI 2.0 YAML source file.
The source can either be a file on the local file system, or a document hosted on a http(s) server e.g. github.
  
All references in the source are are resolved before generating diagrams. Description fields from the JSONSchema objects are added to the generated SVG as a tooltips by using title SVG elements.
  
The object relations are generated for the items based on the 'required' fields in the schema object, also considering min and max item values for JSON arrays.

```text
NAME:
   umldiagram - Generate UML diagrams from JSONSchema objects as SVG images.

USAGE:
    [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --src value          The path to the YAML document containing the Schema object(s). Can be a file path or a http(s):// url scheme.
   --api-version value  The OpenAPI main version of the source file. Default is '2'. Allowed values are '2' and '3'. (default: "2")
   --schema value       The names of the schema objects within the YAML document for which diagrams are to be generated. (eg.: 'myObject1')
   --dir value          The directory where rendered the SVG file(s) are saved. Schema names are used for naming the files. Existing files get overwritten. If left empty, the current directory is used.
   --help, -h           show help

EXAMPLE:

umldiagram --src=https://raw.githubusercontent.com/OpenBankingUK/read-write-api-specs/master/dist/openapi/account-info-openapi.yaml \
   --schema=OBAccount4Detail \
   --schema=OBReadBalance1 \
```

### Installation

#### Compilation from source

Requires Go Programming Language to be installed on the system, and access to the repository.

1. Clone the repository

    `git clone git@bitbucket.org:openbankingteam/uml-generator.git`

2. Navigate to the directory where the executable is and build the binary

    `cd uml-generator/object/cmd/umldiagram`  
    `go install` to have the compiled binary placed in $GOBIN directory, or `go build` to build the executable in the current directory.
