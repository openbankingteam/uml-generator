package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strings"

	"bitbucket.org/openbankingteam/uml-generator/object"
	"github.com/urfave/cli"
)

var rename = map[string]string{}

func main() {
	app := &cli.App{
		Action: run,
		Name:   "umldiagram", // TODO: give this a better name
		Usage:  "Generate UML diagrams from JSONSchema objects as SVG images.",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "src",
				Usage: "The path to the YAML document containing the Schema object(s). Can be a file path or a http(s):// url scheme.",
			},
			&cli.StringFlag{
				Name:  "api-version",
				Usage: "The OpenAPI main version of the source file. Default is '2'. Allowed values are '2' and '3'.",
				Value: "2",
			},
			&cli.StringSliceFlag{
				Name:  "schema",
				Usage: "The names of the schema objects within the YAML document for which diagrams are to be generated. (eg.: 'myObject1')",
			},
			&cli.StringFlag{
				Name:  "dir",
				Usage: "The directory where rendered the SVG file(s) are saved. Schema names are used for naming the files. Existing files get overwritten. If left empty, the current directory is used.",
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func run(ctx *cli.Context) error {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	srcPath := ctx.String("src")
	var src io.ReadCloser
	var err error

	if strings.HasPrefix(srcPath, "http") {
		src, err = openRemoteSrc(srcPath)
		if err != nil {
			return err
		}
	} else {
		src, err = openFileSrc(srcPath)
		if err != nil {
			return err
		}
	}

	// TODO: this could be automatically recognised from the source file
	// TBD: support sources where objects are not separated / refereced but are inline in the endpoints sections
	var objectsPath string
	switch version := ctx.String("api-version"); version {
	case "2":
		objectsPath = "definitions"
	case "3":
		objectsPath = "components.schemas"
	// case "-":
	// TODO: any object in the source
	default:
		return fmt.Errorf("unknown OpenAPI version: %v", version)
	}

	defer src.Close()
	m, err := object.ParseToMap(src)
	if err != nil {
		return err
	}

	for _, name := range ctx.StringSlice("schema") {
		// rename if schema ends with ':newName'
		segments := strings.Split(name, ":")
		if len(segments) == 2 {
			rename[segments[0]] = segments[1]
			name = segments[0]
		}

		path := strings.Join([]string{objectsPath, name}, object.ExternalDivider)
		objectToRender := object.GetObject(m, path)
		if len(objectToRender) == 0 {
			log.Printf("object not found: %s", path)
			continue
		}

		// objNameSegments := strings.Split(name, object.ExternalDivider)
		// objName := objNameSegments[len(objNameSegments)-1]
		d, err := object.MakeDiagram(getName(name), "", objectToRender)
		if err != nil {
			log.Printf("failed to generate diagram for %s: %s", name, err)
			continue
		}

		err = saveDiagram(ctx, d, getName(name))
		if err != nil {
			log.Println(err)
			continue
		}
	}

	return nil
}

func getName(name string) string {
	if n, ok := rename[name]; ok {
		return n
	}
	return name
}

func saveDiagram(ctx *cli.Context, d *object.Diagram, name string) error {
	dir := ctx.String("dir")
	if len(dir) > 0 {
		err := os.MkdirAll(dir, os.ModeDir|0755)
		if err != nil {
			return err
		}
	} else {
		dir = "."
	}

	dst, err := os.Create(path.Join(dir, name) + ".svg")
	if err != nil {
		return err
	}

	defer dst.Close()
	return d.Render(dst)
}

func openFileSrc(filePath string) (io.ReadCloser, error) {
	object.SchemaSourceDir = path.Dir(filePath)
	return os.Open(filePath)
}

func openRemoteSrc(url string) (io.ReadCloser, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode/100 != 2 {
		return nil, fmt.Errorf("remote retuned status %v", resp.StatusCode)
	}

	return resp.Body, nil
}
