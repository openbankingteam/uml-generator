package object

import (
	"fmt"
	"io"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"

	"gopkg.in/yaml.v3"
)

var (
	// ExternalDivider can be set to custom character in places where . is escaped
	ExternalDivider = "."
	internalDivider = "."

	// SchemaSourceDir sets the root path for resolving schemas when the sources (YAML) are in multiple files
	SchemaSourceDir = "."
)

// ParseToDiagram performs all the necessary steps for creating a diagram in one function.
// objectPath must be pointing to a valid jsonSchema object in any JSON document.
func ParseToDiagram(src io.Reader, objectPath string, objectDescription string) (*Diagram, error) {
	m, err := ParseToMap(src)
	if err != nil {
		return nil, err
	}

	objectPath = strings.ReplaceAll(objectPath, ExternalDivider, internalDivider)

	objectToRender := Get(m, objectPath)
	if m, ok := objectToRender.(map[string]interface{}); ok {
		psegs := strings.Split(objectPath, internalDivider)
		name := psegs[len(psegs)-1]
		description := ""

		if desc, ok := m["description"].(string); ok {
			description = desc
		}
		return MakeDiagram(name, description, m)
	}
	return nil, fmt.Errorf("objectPath points to a non object type")
}

// MakeDiagram from a map[string]interface{} representation of a valid JSONSchema object
func MakeDiagram(name, _ string, rootObject map[string]interface{}) (*Diagram, error) {
	// TODO: use description for root object
	diagram := &Object{}
	switch rootObject["type"] {
	case "object":
		if err := parseObjectProperties(name, rootObject, diagram, nil); err != nil {
			return nil, err
		}

	case "array":
		if err := parseArrayProperties(name, rootObject, diagram, nil); err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("failed to render diagram for '%s': unsupported root type '%s'", name, rootObject["type"])

	}

	return &Diagram{Root: diagram.ComposedOf[0].Object}, nil
}

// ParseToMap the selected objectPath. If ObjectPath is the root element of the jsonschema document
// then all references are going to be resolved in the returned map. This map can be reused with MakeDiagram
// to generate multiple diagrams from the same source without unmarshalling / resolving references each time.
func ParseToMap(src io.Reader) (map[string]interface{}, error) {
	m, err := unmarshalSrc(src)
	if err != nil {
		return nil, err
	}

	// resolve $ref items and replace them with the actual definitions
	return resolveAllReferences(m)
}

// map[string]interface{} is iterated in random order which can be confusing.
// Iterable can provide the same values as slice members sorted alphabetically
type iterable []iterItem
type iterItem struct {
	Key   string
	Value interface{}
}

func (it iterable) Len() int           { return len(it) }
func (it iterable) Swap(i, j int)      { it[i], it[j] = it[j], it[i] }
func (it iterable) Less(i, j int) bool { return it[i].Key < it[j].Key }

func mapToIter(m map[string]interface{}) iterable {
	items := make(iterable, 0, len(m))
	for k, v := range m {
		items = append(items, iterItem{Key: k, Value: v})
	}
	sort.Sort(&items)
	return items
}

type relation struct {
	min int // min default means 0
	max int // max default means * (deliberately defining 0 as maximum would make the field useless)
}

func (r *relation) String() string {
	maxStr := "*"
	if r.max > 0 {
		maxStr = strconv.Itoa(r.max)
	}

	return fmt.Sprintf("%v..%s", r.min, maxStr)
}

func parseArrayProperties(name string, arrayItem map[string]interface{}, parent *Object, rel *relation) error {
	arrayItem = GetObject(arrayItem, "items")
	if arrayItem == nil {
		return fmt.Errorf("'items' field not found for array type")
	}

	switch arrayItem["type"] {
	case "object":
		if err := parseObjectProperties(name, arrayItem, parent, rel); err != nil {
			return err
		}

	case "array":
		return fmt.Errorf("nested arrays are not implemented")

	default:
		rel := parseRelation(arrayItem, name)
		_ = parseScalarProperty(name, arrayItem, parent, rel)
	}
	return nil
}

// objectItem is the child object jsonschema unmarshalled to map[string]interface{}
// parent is the rendered parent object which is composed of the object that's being parsed
// required is used to create the relation between parent and parsed objects.
func parseObjectProperties(name string, objectItem map[string]interface{}, parent *Object, rel *relation) error {
	properties := GetObject(objectItem, "properties")
	if properties == nil {
		// don't draw empty objects
		return nil
	}

	child := &Object{Name: name}
	if desc, ok := objectItem["description"].(string); ok && len(desc) > 0 {
		child.Description = desc
	}

	if parent != nil {
		composeObject(parent, child, rel)
	}

	for _, childProperty := range mapToIter(properties) {
		cp := childProperty.Value.(map[string]interface{})
		switch cp["type"] {
		case "object":
			rel := parseRelation(objectItem, childProperty.Key)
			if err := parseObjectProperties(childProperty.Key, cp, child, rel); err != nil {
				return err
			}

		case "array":
			rel := parseArrayRelation(objectItem, childProperty.Key)
			if err := parseArrayProperties(childProperty.Key, cp, child, rel); err != nil {
				return err
			}

		default:
			rel := parseRelation(objectItem, childProperty.Key)
			_ = parseScalarProperty(childProperty.Key, cp, child, rel)
		}
	}

	return nil
}

func parseScalarProperty(name string, scalarItem map[string]interface{}, parent *Object, rel *relation) error {
	info := strings.Builder{}
	for _, key := range []string{"type", "format", "minLength", "maxLength", "description", "enum", "x-namespaced-enum", "pattern"} {
		switch key {
		case "enum", "x-namespaced-enum":
			if value := scalarItem[key]; value != nil {
				info.WriteString("Values:\n")
				for _, v := range value.([]interface{}) {
					info.WriteString(fmt.Sprintf(" - %s\n", v))
				}
			}
		default:
			if scalarItem[key] != nil {
				value := fmt.Sprint(scalarItem[key])
				if len(value) > 0 {
					info.WriteString(fmt.Sprintf("%s: %s\n", strings.Title(key), value))
				}
			}
		}
	}

	newProperty := Property{
		Name:         name,
		Relationship: fmt.Sprint(rel),
	}

	if desc := info.String(); len(desc) > 0 {
		newProperty.Description = desc
	}

	parent.Properties = append(parent.Properties, newProperty)
	return nil
}

func parseArrayRelation(parentObject map[string]interface{}, key string) *relation {
	minKey := strings.Join([]string{"properties", key, "minItems"}, internalDivider)
	maxKey := strings.Join([]string{"properties", key, "maxItems"}, internalDivider)
	rel := &relation{
		int(GetNumber(parentObject, minKey)),
		int(GetNumber(parentObject, maxKey)),
	}

	if isRequiredField(parentObject, key) {
		rel.min = 1
	}

	return rel
}

func parseRelation(parentObject map[string]interface{}, key string) *relation {
	rel := &relation{0, 1}
	if isRequiredField(parentObject, key) {
		rel.min = 1
	}
	return rel
}

// m is the map with the unmarshalled root schema of the object with the property
// containing the "required" field. name is the name of the field.
func isRequiredField(m map[string]interface{}, name string) bool {
	req, _ := m["required"].([]interface{})
	if req == nil {
		return false
	}

	for _, v := range req {
		if strings.EqualFold(v.(string), name) {
			return true
		}
	}

	return false
}

func composeObject(parent, child *Object, rel *relation) {
	parent.ComposedOf = append(parent.ComposedOf, Composition{
		Relationship: fmt.Sprint(rel),
		Object:       child,
	})
}

func resolveAllReferences(m map[string]interface{}) (map[string]interface{}, error) {
	keys := make([]string, 0, len(m))
	for key := range m {
		keys = append(keys, key)
	}

	dst := map[string]interface{}{}
	for _, k := range keys {
		resolved, err := resolveReferences(m, k)
		if err != nil {
			return nil, err
		}
		dst[k] = resolved
	}

	return dst, nil
}

func resolveReferences(m map[string]interface{}, path string) (interface{}, error) {
	src := Get(m, path)
	var err error

	switch t := src.(type) {
	case map[string]interface{}:
		dst := map[string]interface{}{}
		for k, v := range t {
			if k == "$ref" {
				nextObject := m
				filePath, objectPath, err := parseReferenceString(v.(string))
				if err != nil {
					return nil, err
				}

				if len(filePath) > 0 {
					nextObject, err = getObjectFromFile(filePath)
					if err != nil {
						return nil, err
					}
				}

				// safe to return, $ref can only appear in recursion
				return resolveReferences(nextObject, objectPath)
			}
			subpath := strings.Join([]string{path, k}, internalDivider)
			dst[k], err = resolveReferences(m, subpath)
			if err != nil {
				return nil, err
			}
		}
		return dst, nil

	case []interface{}:
		dst := make([]interface{}, len(t))
		for i := range t {
			subpath := strings.Join([]string{path, fmt.Sprint(i)}, internalDivider)
			dst[i], err = resolveReferences(m, subpath)
			if err != nil {
				return nil, err
			}
		}
		return dst, nil

	case nil:
		return nil, fmt.Errorf("reference '%s' not found", path)

	default:
		return t, nil
	}
}

func parseReferenceString(ref string) (filePath, objectPath string, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("error parsing ref from '%s': %v", ref, r)
		}
	}()

	segments := strings.Split(ref, "#")
	filePath = segments[0]
	objectPath = strings.ReplaceAll(strings.Trim(segments[1], "/"), "/", internalDivider)
	return
}

func getObjectFromFile(filePath string) (map[string]interface{}, error) {
	f, err := os.Open(path.Join(SchemaSourceDir, filePath))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	o := map[string]interface{}{}
	dec := yaml.NewDecoder(f)
	err = dec.Decode(&o)
	return o, err
}
