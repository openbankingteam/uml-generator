package object

import (
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"strings"

	"gopkg.in/yaml.v3"
)

func unmarshalSrc(src io.Reader) (map[string]interface{}, error) {
	dst := map[string]interface{}{}
	srcBody, err := ioutil.ReadAll(src)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(srcBody, &dst); err != nil {
		return nil, err
	}

	return dst, nil
}

// Get the item as an interface{}
func Get(m map[string]interface{}, key string) interface{} {
	fieldValue, found := getField(m, key)
	if !found {
		return nil
	}

	return fieldValue
}

// GetString returns the item as a string. If the item is not a string, the string representation is returned.
// It is recommended to use the helpers only in cases where the type of the target item is known.
func GetString(m map[string]interface{}, key string) string {
	fieldValue, _ := getField(m, key)
	stringValue, ok := fieldValue.(string)
	if ok {
		return stringValue
	}

	if fieldValue != nil {
		return fmt.Sprint(fieldValue)
	}
	return ""
}

// GetNumber returns the item as float64. If the item is not parseable as number, 0.0 is returned.
// It is recommended to use the helpers only in cases where the type of the target item is known.
func GetNumber(m map[string]interface{}, key string) float64 {
	fieldValue, _ := getField(m, key)
	if number, ok := fieldValue.(float64); ok {
		return number
	}

	return 0.0
}

// GetSlice returns the item as a slice of interface{} values.
// It is recommended to use the helpers only in cases where the type of the target item is known.
func GetSlice(m map[string]interface{}, key string) []interface{} {
	fieldValue, _ := getField(m, key)
	if slice, ok := fieldValue.([]interface{}); ok {
		return slice
	}

	return []interface{}{}
}

// GetObject returns the item as a map[string]interface{}.
// It is recommended to use the helpers only in cases where the type of the target item is known.
func GetObject(m map[string]interface{}, key string) map[string]interface{} {
	fieldValue, _ := getField(m, key)
	if obj, ok := fieldValue.(map[string]interface{}); ok {
		return obj
	}

	return map[string]interface{}{}
}

func getField(v interface{}, key string) (interface{}, bool) {
	segment, key := splitKey(key)
	if segment == "" {
		return v, true
	}

	switch t := v.(type) {
	case []interface{}:
		idx, err := strconv.Atoi(segment)
		if err != nil || idx >= len(t) || idx < 0 {
			return nil, false
		}
		return getField(t[idx], key)

	case map[string]interface{}:
		v, found := t[segment]
		if !found {
			return nil, false
		}
		return getField(v, key)
	}
	return nil, false
}

func splitKey(key string) (string, string) {
	key = strings.Trim(key, internalDivider)
	idx := strings.Index(key, internalDivider)
	if idx == -1 {
		return key, ""
	}
	return key[:idx], key[idx+1:]
}
