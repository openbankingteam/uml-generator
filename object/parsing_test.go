package object

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	openAPI2TestFile = "test-example-openapi2.yaml"
	openAPI3TestFile = "test-example-openapi3.yaml"
)

func getTestFileContents(path ...string) ([]io.Reader, error) {
	testInputs := make([]io.Reader, len(path))
	for i, testFile := range path {
		b, err := ioutil.ReadFile(testFile)
		if err != nil {
			return nil, err
		}
		testInputs[i] = bytes.NewBuffer(b)
	}
	return testInputs, nil
}

func TestParseToMap(t *testing.T) {
	testInputs, err := getTestFileContents(openAPI2TestFile, openAPI3TestFile)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		Name          string
		Input         io.Reader
		ExpectedItems int
		ExpectedError error
	}{
		{
			"Parsing OpenAPI2",
			testInputs[0],
			11,
			nil,
		},
		{
			"Parsing OpenAPI3",
			testInputs[1],
			5,
			nil,
		},
	}

	for _, tc := range testCases {
		t.Log(tc.Name)
		m, err := ParseToMap(tc.Input)
		if err != tc.ExpectedError {
			t.Fatalf("FAIL: %s - expected error value: '%s', actual error: '%s'",
				tc.Name, tc.ExpectedError, err)
		}

		assert.Len(t, m, tc.ExpectedItems)
		// TODO: assertions on parsed items to check internal structure
	}
}

func TestMakeDiagram(t *testing.T) {
	testInputs, err := getTestFileContents(openAPI2TestFile, openAPI3TestFile)
	if err != nil {
		t.Fatal(err)
	}
	if err != nil {
		t.Fatal(err)
	}

	// objects that exists in the external test-example... files.
	testObjects := []string{
		"OBReadAccount6",                         // object
		"OBBCAData1",                             // object with nested objects and arrays
		"OBBCAData1.properties.OtherFeesCharges", // array
		"OBReadBalance1.properties.Data.properties.Balance.items.properties.Amount", // object
	}

	testCases := []struct {
		Name                  string
		OpenAPIVersion        string
		Paths                 []string
		ExpectedBoxesRendered []int
		ExpectedError         error
	}{
		{
			"Using OpenAPI2 source",
			"2.0",
			testObjects,
			[]int{7, 44, 13, 1},
			nil,
		},
		{
			"Using OpenAPI3 source",
			"3.0",
			testObjects,
			[]int{7, 44, 13, 1},
			nil,
		},
	}

	// struct to unmarshal svg to, for asserting element counts in it
	type diagram struct {
		XMLName xml.Name `xml:"svg"`
		Rect    []struct {
			X string `xml:"x,attr"`
			Y string `xml:"y,attr"`
		} `xml:"rect"`
		Text []struct {
			Contents string `xml:",chardata"`
			Title    string `xml:"title"`
		} `xml:"text"`
	}

	for _, tc := range testCases {
		var input io.Reader
		var pathPrefix string

		switch tc.OpenAPIVersion {
		case "2.0":
			input = testInputs[0]
			pathPrefix = "definitions."
		case "3.0":
			input = testInputs[1]
			pathPrefix = "components.schemas."
		}

		m, err := ParseToMap(input)
		if err != nil {
			t.Fatal(err)
		}

		for i, path := range tc.Paths {
			path = pathPrefix + path
			diagramObject := GetObject(m, path)
			pathSegments := strings.Split(path, ".")
			name := pathSegments[len(pathSegments)-1]
			d, err := MakeDiagram(name, "", diagramObject)
			if err != nil {
				t.Fatal(err, tc.Name)
			}

			rendered := &bytes.Buffer{}
			err = d.Render(rendered)
			if err != nil {
				t.Fatal(err, tc.Name, name)
			}

			// uncomment for debugging
			// ioutil.WriteFile(name+".svg", rendered.Bytes(), 0775)

			extractedElements := &diagram{}
			err = xml.Unmarshal(rendered.Bytes(), extractedElements)
			if err != nil {
				t.Fatal(err, tc.Name, name)
			}

			assert.Equal(t, tc.ExpectedBoxesRendered[i], len(extractedElements.Rect), fmt.Sprintf("test: %s\nelement: %s", tc.Name, name))
			// TODO: more complete assertion set for component details / parameters
		}
	}
}
